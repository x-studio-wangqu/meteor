<#--
/****************************************************
 * Description: 服务器信息的简单列表页面，没有编辑功能
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-04 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-list.ftl">
<@list id=tabId>
    <thead>
    <tr>
        <th><input type="checkbox" class="bscheckall"></th>
        <th>机器名称</th>
        <th>登录类型</th>
        <th>RSA_ID</th>
        <th>登陆账号</th>
        <th>SSH端口号</th>
        <th>agentIP</th>
        <th>agent端口</th>
        <th>agentId</th>
        <th>模块名称</th>
        <th>状态</th>
        <th>创建时间</th>
        <th>创建人员ID</th>
        <th>操作人员名称</th>
        <th>连接状态</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <#list page.items?if_exists as item>
        <tr>
            <td>
                <input type="checkbox" class="bscheck" data="id:${item.id}">
            </td>
            <td>
                ${item.hostname}

            </td>
            <td>
                ${item.loginType}

            </td>
            <td>
                ${item.rsaId}

            </td>
            <td>
                ${item.username}

            </td>
            <td>
                ${item.port}

            </td>
            <td>
                ${item.arthasIp}

            </td>
            <td>
                ${item.arthasPort}

            </td>
            <td>
                ${item.arthasAgentId}

            </td>
            <td>
                ${item.moduleName}

            </td>
            <td>
                <span class="label <#if item.status=XJJConstants.COMMON_STATUS_VALID>label-info</#if> arrowed-in arrowed-in-right">${XJJDict.getText(item.status)}</span>

            </td>
            <td>
                ${item.createTime?string('yyyy-MM-dd HH:mm:ss')}
            </td>
            <td>
                ${item.createPersonId}

            </td>
            <td>
                ${item.createPersonName}

            </td>
            <td>
                ${DictConstants.getDictName('machine',item.serverStatus)}
            </td>
            <td>
                <@button type="info" icon=" fa fa-user" onclick="initMachine('${item.id}');">初始化</@button>
                <#if item.status=XJJConstants.COMMON_STATUS_VALID>
                    <@button type="danger" icon="fa fa-trash-o" onclick="XJJ.edit('${base}/meteor/machineinfo/status/${item.id}','修改开启状态','${tabId}');">关闭</@button>
                <#else>
                    <@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/meteor/machineinfo/status/${item.id}','修改开启状态','${tabId}');">开启</@button>
                </#if>
                <@button type="info" icon=" fa fa-user" onclick="XJJ.view('${base}/meteor/machineinfo/privilege/allot/${item.id}','为【${item.hostname}】设置权限','${tabId}');">添加人员</@button>
            </td>
        </tr>
    </#list>
    </tbody>
</@list>

<script type="text/javascript">
    function initMachine(machineId) {
        var url = "${base}/meteor/machineinfo/initMachine/" + machineId;
        $.ajax({
            type: "GET",
            async: false,
            dataType: "JSON",
            contentType: "application/json;charset=UTF-8",
            url: url,
            success: function (data) {
                if (data.message != '' && data.type == 'success') {
                    XJJ.msgok(data.message);
                    XJJ.query({id: 'meteor_machineinfo'});
                    //开启arthas
                    XJJ.view("${base}/meteor/machineinfo/agent/" + machineId, "初始化Meteor-Agent");
                } else {
                    XJJ.msger(data.message);
                }
            },
            error: function (e) {
                XJJ.msger(e.responseText);
                console.log(e.status);
                console.log(e.responseText);
            }

        });
    }
</script>
