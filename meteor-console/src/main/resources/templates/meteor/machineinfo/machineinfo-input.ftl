<#--
/****************************************************
 * Description: 服务器信息的输入页面，包括添加和修改
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-04 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@input url="${base}/meteor/machineinfo/save" id=tabId>
    <input type="hidden" name="id" value="${machineinfo.id}"/>
    <@formgroup title='机器名称'>
        <input type="text" name="hostname" value="${machineinfo.hostname}" check-type="required">
    </@formgroup>
    <@formgroup title='是否免密登录'>
        <@swichInForm name="loginType" val=machineinfo.loginType onVal="rsa" offVal="nrsa" onTitle="免密登录" offTitle="账号登录"></@swichInForm>
    </@formgroup>
    <div id="rsa">
        <@formgroup title='RSA_ID'>
            <@select id="rsaId" name="rsaId" value=machineinfo.rsaId list=rsaList listKey="rsaName" listValue="id"></@select>
        </@formgroup>
    </div>
    <div id="nrsa">
        <@formgroup title='登陆账号'>
            <input type="text" name="username" value="${machineinfo.username}" check-type="required">
        </@formgroup>
        <@formgroup title='密码'>
            <input type="password" name="password" value="${machineinfo.password}" check-type="required">
        </@formgroup>
    </div>

    <@formgroup title='远程连接端口号'>
        <input type="text" name="port"
               value="<#if (machineinfo.port)?? && machineinfo.port!=''>${machineinfo.port}<#else>22</#if>"
               check-type="number">
    </@formgroup>
    <@formgroup title='agentIP/proxyIP'>
        <input type="text" name="arthasIp"
               value="<#if (machineinfo.arthasIp)?? && machineinfo.arthasIp!=''>${machineinfo.arthasIp}<#else>${hostName}</#if>">
    </@formgroup>
    <@formgroup title='agent端口'>
        <input type="text" name="arthasPort"
               value="<#if (machineinfo.arthasPort)?? && machineinfo.arthasPort!=''>${machineinfo.arthasPort}<#else>7777</#if>"
               check-type="required number">
    </@formgroup>
    <@formgroup title='agentId'>
        <input type="text" name="arthasAgentId" check-type="required"
               value="<#if (machineinfo.arthasAgentId)?? && machineinfo.arthasAgentId!=''>${machineinfo.arthasAgentId}<#else>${arthasAgentId}</#if>">
    </@formgroup>
    <@formgroup title='模块名称'>
        <input type="text" name="moduleName" value="${machineinfo.moduleName}" check-type="required">
    </@formgroup>
    <@formgroup title='状态'>
        <@swichInForm name="status" val=machineinfo.status onVal="valid" offVal="invalid" onTitle="有效" offTitle="无效"></@swichInForm>
    </@formgroup>
    <@formgroup title='创建时间'>
        <@datetime  dateValue=machineinfo.createTime required="required" default=true/>
    </@formgroup>
    <@formgroup title='创建人员ID'>
        <input type="text" name="createPersonId"
               value="<#if (machineinfo.createPersonId)?? && machineinfo.createPersonId!=''>${machineinfo.createPersonId}<#else>${session_manager_info_key.userId}</#if>"
               check-type="required" readonly="readonly">
    </@formgroup>
    <@formgroup title='操作人员名称'>
        <input type="text" name="createPersonName"
               value="<#if (machineinfo.createPersonName)?? && machineinfo.createPersonName!=''>${machineinfo.createPersonName}<#else>${session_manager_info_key.userName}</#if>"
               check-type="required" readonly="readonly">
    </@formgroup>
</@input>
<script type="text/javascript">
    $(function () {
        var value = $("input[name='loginType']").val();
        if (value == 'rsa') {
            $("#rsa").show();
            $("#nrsa").hide();
            $("input[name='rsaId']").attr("check-type", "required");
            $("input[name='username']").removeAttr("check-type");
            $("input[name='password']").removeAttr("check-type");
        } else {
            $("#rsa").hide();
            $("#nrsa").show();
            $("input[name='rsaId']").removeAttr("check-type");
            $("input[name='username']").attr("check-type", "required");
            $("input[name='password']").attr("check-type", "required");
        }
        $("#switch-loginType").click(function () {
            if ($(this).is(":checked")) {
                $("#rsa").show();
                $("#nrsa").hide();
                $("input[name='rsaId']").attr("check-type", "required");
                $("input[name='username']").removeAttr("check-type");
                $("input[name='password']").removeAttr("check-type");
            } else {
                $("#rsa").hide();
                $("#nrsa").show();
                $("input[name='rsaId']").removeAttr("check-type");
                $("input[name='username']").attr("check-type", "required");
                $("input[name='password']").attr("check-type", "required");
            }
        });
    });
</script>
