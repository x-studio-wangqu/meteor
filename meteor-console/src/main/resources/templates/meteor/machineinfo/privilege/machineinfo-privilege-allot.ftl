<#include "/templates/xjj-index.ftl">
<@input>
    <input type="hidden" id="machineId" value="${machineId}"/>
    <div class="table-responsive">
        <@formgroup  title='人员'>
            <@select id="userId" value=userIdList double=true list=userList listKey="userName" listValue="id" checkType="required" onChange="modifyUserList()"></@select>
        </@formgroup>

    </div>
</@input>
<script type="text/javascript">

    function modifyUserList() {
        var machineId = $("#machineId").val();
        var url = "${base}/meteor/machineinfo/privilege/save";
        var userId = $("#userId").val();
        $.ajax({
            type: "post",
            url: url,
            dataType: "JSON",
            contentType: "application/json;charset=UTF-8",
            data: JSON.stringify({"machineId": machineId, "userId": userId}),
            success: function (data) {
                if (data.message != '' && data.type == 'success') {
                    XJJ.msgok(data.message);
                } else {
                    XJJ.msger(data.message);
                }
            },
            error: function (e) {
                XJJ.msger(e.responseText);
                console.log(e.status);
                console.log(e.responseText);
            }
        });
    }
</script>


