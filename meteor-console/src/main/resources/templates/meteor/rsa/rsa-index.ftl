<#--
/****************************************************
 * Description: 私钥信息的列表页面
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-12-13 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>
<@content id=tabId>
	<@query queryUrl="${base}/meteor/rsa/list" id=tabId>
		<@querygroup title='名称'>
			<input type="search" name="query.name@lk@s" class="form-control input-sm" placeholder="请输入名称" aria-controls="dynamic-table">
	    </@querygroup>
		<@querygroup title='状态'>
			<@select name="query.status@eq@s" list=XJJConstants.COMMON_STATUS_LIST></@select>
	    </@querygroup>
		<@button type="info" icon="glyphicon glyphicon-search" onclick="XJJ.query({id:'${tabId}'});">查询</@button>
	</@query>


	<@button type="info" icon="glyphicon glyphicon-plus" onclick="XJJ.add('${base}/meteor/rsa/input','添加私钥信息','${tabId}');">增加</@button>
	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/meteor/rsa/input','修改私钥信息','${tabId}');">修改</@button>
	<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/meteor/rsa/delete','删除私钥信息？',true,{id:'${tabId}'});">删除</@button>

	<@button type="success" icon="fa fa-cloud-upload" onclick="XJJ.add('${base}/meteor/rsa/import','导入','${tabId}');">导入</@button>
	<@button type="success" icon="fa fa-cloud-download" onclick="window.location.href='${base}/meteor/rsa/export/excel';">导出</@button>

</@content>
