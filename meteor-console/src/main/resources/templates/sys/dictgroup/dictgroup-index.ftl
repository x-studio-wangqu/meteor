<#--
/****************************************************
 * Description: 字典组管理的列表页面
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author      reywong
 * @version     1.0
 * @see
	HISTORY
    *  2019-08-16 reywong Create File
**************************************************/
-->
<#include "/templates/xjj-index.ftl">
<@navList navs=navArr/>

<@content id=tabId>
	<@query queryUrl="${base}/sys/dictgroup/list" id=tabId>
		<@querygroup title='组编码'>
			<input type="search" name="query.groupCodeM@lk@s" class="form-control input-sm" placeholder="请输入名称" aria-controls="dynamic-table">
		</@querygroup>

		<@querygroup title='组名'>
			<input type="search" name="query.groupName@lk@s" class="form-control input-sm" placeholder="请输入名称" aria-controls="dynamic-table">
	    </@querygroup>

		<@button type="info" icon="glyphicon glyphicon-search" onclick="XJJ.query({id:'${tabId}'});">查询</@button>
	</@query>


	<@button type="info" icon="glyphicon glyphicon-plus" onclick="XJJ.add('${base}/sys/dictgroup/input','添加字典组管理','${tabId}');">增加</@button>
	<@button type="purple" icon="fa fa-pencil" onclick="XJJ.edit('${base}/sys/dictgroup/input','修改字典组管理','${tabId}');">修改</@button>
	<@button type="danger" icon=" fa fa-trash-o" onclick="XJJ.del('${base}/sys/dictgroup/delete','删除字典组管理？',true,{id:'${tabId}'});">删除</@button>

	<@button type="grey" icon="fa fa-cloud-upload">上传</@button>
</@content>
