package cn.com.ry.framework.application.meteor.sec.web;

import cn.com.ry.framework.application.meteor.framework.XJJConstants;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.*;
import cn.com.ry.framework.application.meteor.framework.utils.EncryptUtils;
import cn.com.ry.framework.application.meteor.framework.utils.Excel2007Util;
import cn.com.ry.framework.application.meteor.framework.utils.StringUtils;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.framework.web.support.Pagination;
import cn.com.ry.framework.application.meteor.framework.web.support.QueryParameter;
import cn.com.ry.framework.application.meteor.framework.web.support.XJJParameter;
import cn.com.ry.framework.application.meteor.sec.entity.RoleEntity;
import cn.com.ry.framework.application.meteor.sec.entity.XjjUser;
import cn.com.ry.framework.application.meteor.sec.service.RoleService;
import cn.com.ry.framework.application.meteor.sec.service.UserRoleService;
import cn.com.ry.framework.application.meteor.sec.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/sec/manager")
public class ManagerController extends SpringControllerSupport {

    @Autowired
    private UserService userService;

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserRoleService userRoleService;

    @SecPrivilege(title = "管理员管理")
    @RequestMapping(value = "/index")
    public String index(Model model) {
        String page = this.getViewPath("index");
        return page;
    }

    @SecList
    @RequestMapping(value = "/list")
    public String list(Model model,
                       @QueryParameter XJJParameter query,
                       @ModelAttribute("page") Pagination page
    ) {
        //查询的用户类型集合
        query.addQuery("query.userType@eq@s", XJJConstants.USER_TYPE_ADMIN);
        query.addOrderByAsc("id");
        page = userService.findPage(query, page);

        return getViewPath("list");
    }

    @SecCreate
    @RequestMapping("/input")
    public String input(@ModelAttribute("user") XjjUser user, Model model) {

        return getViewPath("input");
    }

    /*
     * 修改用户
     */
    @SecEdit
    @RequestMapping("/input/{id}")
    public String edit(@PathVariable("id") Long id, Model model) {
        XjjUser user = userService.getById(id);
        model.addAttribute("user", user);
        return getViewPath("input");
    }


    /*
     * 去添加角色
     */
    @RequestMapping("/role/input/{userId}")
    public String role(@PathVariable("userId") Long userId, Model model) {
        XjjUser user = userService.getById(userId);
        List<RoleEntity> roleList = roleService.findListNoUser(userId);
        List<RoleEntity> roleEntityList = roleService.findAll();
        for (int i = 0; i < roleEntityList.size(); i++) {
            for (int j = 0; j < roleList.size(); i++) {
                if (roleEntityList.get(i).getId() == roleList.get(j).getId()) {
                    roleEntityList.get(i).setCheckStatus("1");
                }
                break;
            }
        }
        model.addAttribute("roleEntityList", roleEntityList);
        model.addAttribute("user", user);
        return getViewPath("role");
    }


    @RequestMapping("/role/save")
    public @ResponseBody
    XjjJson roleSave(
            @RequestParam(value = "userId") Long userId,
            @RequestParam(value = "roleIds") Long[] roleIds) {

        if (null != roleIds && null != userId) {
            userRoleService.deleteAndSave(userId, roleIds);
        }
        return XjjJson.success("保存成功");
    }

    @RequestMapping("/role/cancle/{userId}/{roleId}")
    public @ResponseBody
    XjjJson cancleRole(@PathVariable("userId") Long userId,
                       @PathVariable("roleId") Long roleId) {
        userRoleService.deleteBy2Id(userId, roleId);
        return XjjJson.success("成功删除1条");
    }


    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    @RequestMapping("/save")
    public @ResponseBody
    XjjJson save(@ModelAttribute XjjUser user) {

        if (user.isNew()) {
            user.setCreateDate(new Date());
            userService.save(user);
        } else {
            String password = user.getPassword();
            if (!StringUtils.isBlank(password)) {
                user.setPassword(EncryptUtils.MD5Encode(password));
            }
            userService.update(user);
        }
        return XjjJson.success("保存成功");
    }


    @RequestMapping(value = "/{userId}/detail", method = RequestMethod.POST)
    public String detail(@PathVariable("userId") Long userId, Model model) {
        if (userId == null) {
            return "redirect:/user/list";
        }
        XjjUser user = userService.getById(userId);
        if (user == null) {
            return "forward:/user/list";
        }
        model.addAttribute("user", user);
        return "detail";
    }

    @SecDelete
    @RequestMapping("/delete/{id}")
    public @ResponseBody
    XjjJson delete(@PathVariable("id") Long id) {
        userService.delete(id);
        return XjjJson.success("成功删除1条");
    }

    @SecDelete
    @RequestMapping("/delete")
    public @ResponseBody
    XjjJson delete(@RequestParam("ids") Long[] ids) {
        if (ids == null || ids.length == 0) {
            return XjjJson.error("没有删除");
        }
        for (Long id : ids) {
            userService.delete(id);
        }
        return XjjJson.success("成功删除" + ids.length + "条");
    }


    /**
     * 导入
     *
     * @param model
     * @return
     */
    @RequestMapping(value = "/import/save", method = RequestMethod.POST)
    public @ResponseBody
    XjjJson importSave(Model model, @RequestParam(value = "fileId", required = false) Long fileId) {
        System.out.println("上传开始----");
        try {
            Map<String, Object> map = userService.saveImportUser(fileId);
            int allCnt = (Integer) map.get("allCnt");
            return XjjJson.success("导入成功：本次共计导入数据" + allCnt + "条");
        } catch (ValidationException e) {

            return XjjJson.error("导入失败：<br/>" + e.getMessage());
        }

    }

    /**
     * 导出用户信息
     *
     * @param request
     * @param response
     * @return
     */
    @RequestMapping(value = "/export/excel")
    public String exportExcel(HttpServletRequest request, HttpServletResponse response) {
        List<XjjUser> userList = userService.findAll();
        LinkedHashMap<String, String> columns = new LinkedHashMap<String, String>();
        columns.put("loginName", "账号");
        columns.put("userName", "用户名");
        columns.put("mobile", "手机");
        Excel2007Util.write(userList, columns, response, "user-export");
        return null;
    }

}
