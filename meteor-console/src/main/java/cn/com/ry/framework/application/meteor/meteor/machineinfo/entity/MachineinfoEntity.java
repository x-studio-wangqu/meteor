/****************************************************
 * Description: Entity for 服务器信息
 * Copyright:   Copyright (c) 2019
 * Company:     ry
 * @author reywong
 * @version 1.0
 * @see
HISTORY
 *  2019-12-04 reywong Create File
 **************************************************/

package cn.com.ry.framework.application.meteor.meteor.machineinfo.entity;

import cn.com.ry.framework.application.meteor.framework.entity.EntitySupport;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

public class MachineinfoEntity extends EntitySupport {

    private static final long serialVersionUID = 1L;

    public MachineinfoEntity() {
    }

    private String hostname;//机器名称
    //nrsa/rsa
    private String loginType;
    private Integer rsaId;
    private String username;//登陆账号
    private String password;//密码
    private Integer port;//远程连接端口号
    private String arthasIp;//agentIP
    private Integer arthasPort;//agent端口
    private String arthasAgentId;//agentId
    private String serverStatus;//服务器连接状态
    private String moduleName;//模块名称
    private String status;//状态
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;//创建时间
    private String createPersonId;//创建人员ID
    private String createPersonName;//操作人员名称

    /**
     * 返回机器名称
     *
     * @return 机器名称
     */
    public String getHostname() {
        return hostname;
    }

    /**
     * 设置机器名称
     *
     * @param hostname 机器名称
     */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public Integer getRsaId() {
        return rsaId;
    }

    public void setRsaId(Integer rsaId) {
        this.rsaId = rsaId;
    }

    /**
     * 返回登陆账号
     *
     * @return 登陆账号
     */
    public String getUsername() {
        return username;
    }

    /**
     * 设置登陆账号
     *
     * @param username 登陆账号
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 返回密码
     *
     * @return 密码
     */
    public String getPassword() {
        return password;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 返回远程连接端口号
     *
     * @return 远程连接端口号
     */
    public Integer getPort() {
        return port;
    }

    /**
     * 设置远程连接端口号
     *
     * @param port 远程连接端口号
     */
    public void setPort(Integer port) {
        this.port = port;
    }

    /**
     * 返回agentIP
     *
     * @return agentIP
     */
    public String getArthasIp() {
        return arthasIp;
    }

    /**
     * 设置agentIP
     *
     * @param arthasIp agentIP
     */
    public void setArthasIp(String arthasIp) {
        this.arthasIp = arthasIp;
    }

    /**
     * 返回agent端口
     *
     * @return agent端口
     */
    public Integer getArthasPort() {
        return arthasPort;
    }

    /**
     * 设置agent端口
     *
     * @param arthasPort agent端口
     */
    public void setArthasPort(Integer arthasPort) {
        this.arthasPort = arthasPort;
    }

    /**
     * 返回agentId
     *
     * @return agentId
     */
    public String getArthasAgentId() {
        return arthasAgentId;
    }

    /**
     * 设置agentId
     *
     * @param arthasAgentId agentId
     */
    public void setArthasAgentId(String arthasAgentId) {
        this.arthasAgentId = arthasAgentId;
    }

    /**
     * 返回服务器连接状态
     *
     * @return 服务器连接状态
     */
    public String getServerStatus() {
        return serverStatus;
    }

    /**
     * 设置服务器连接状态
     *
     * @param serverStatus 服务器连接状态
     */
    public void setServerStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }

    /**
     * 返回模块名称
     *
     * @return 模块名称
     */
    public String getModuleName() {
        return moduleName;
    }

    /**
     * 设置模块名称
     *
     * @param moduleName 模块名称
     */
    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    /**
     * 返回状态
     *
     * @return 状态
     */
    public String getStatus() {
        return status;
    }

    /**
     * 设置状态
     *
     * @param status 状态
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     * 返回创建时间
     *
     * @return 创建时间
     */
    public Date getCreateTime() {
        return createTime;
    }

    /**
     * 设置创建时间
     *
     * @param createTime 创建时间
     */
    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    /**
     * 返回创建人员ID
     *
     * @return 创建人员ID
     */
    public String getCreatePersonId() {
        return createPersonId;
    }

    /**
     * 设置创建人员ID
     *
     * @param createPersonId 创建人员ID
     */
    public void setCreatePersonId(String createPersonId) {
        this.createPersonId = createPersonId;
    }

    /**
     * 返回操作人员名称
     *
     * @return 操作人员名称
     */
    public String getCreatePersonName() {
        return createPersonName;
    }

    /**
     * 设置操作人员名称
     *
     * @param createPersonName 操作人员名称
     */
    public void setCreatePersonName(String createPersonName) {
        this.createPersonName = createPersonName;
    }


    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SIMPLE_STYLE).append("cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoEntity").append("ID=" + this.getId()).toString();
    }
}

