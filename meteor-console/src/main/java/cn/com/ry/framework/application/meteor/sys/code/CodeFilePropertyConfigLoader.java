package cn.com.ry.framework.application.meteor.sys.code;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class CodeFilePropertyConfigLoader {

    protected static final Logger logger = Logger.getLogger(CodeFilePropertyConfigLoader.class);

    private static String fileName = "/config/code.properties";

    public static Properties load() {
        Properties properties = new Properties();
        try {
            InputStream is = CodeFilePropertyConfigLoader.class.getResourceAsStream(fileName);
            properties.load(is);
        } catch (Exception e) {
            //e.printStackTrace();
            //throw new Exception("Can't Read Properties from File:" + fileName);
            if (logger.isDebugEnabled()) {
                logger.debug("Can't Read Properties from File:" + fileName);
            }
        }
        return properties;
    }

    public void save(Properties properties) throws Exception {
        try {
            File propFile = new File(this.fileName);
            properties.store(new FileOutputStream(propFile), "");
        } catch (IOException e) {
            e.printStackTrace();
            throw new Exception("Can't Save Properties to File:" + fileName);
        }
    }
}
