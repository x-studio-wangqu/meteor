package cn.com.ry.framework.application.meteor.meteor.appinfo.web;

import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/meteor/appinfo")
public class AppinfoController extends SpringControllerSupport {

    @SecPrivilege(title = "应用管理")
    @SecList
    @RequestMapping(value = "/index")
    public String index(Model model) {
        String page = this.getViewPath("index");
        return page;
    }

    @RequestMapping(value = "/help")
    public String help(Model model) {
        String page = this.getViewPath("help");
        return page;
    }

    @RequestMapping(value = "/terminal/{cmd}/{appinfo_ip}/{appinfo_port}/{appinfo_agentId}")
    public String terminal(Model model, @PathVariable String cmd, @PathVariable String appinfo_ip, @PathVariable String appinfo_port, @PathVariable String appinfo_agentId) {
        String page = this.getViewPath("terminal");
        model.addAttribute("cmd", cmd);
        model.addAttribute("appinfo_ip", appinfo_ip);
        model.addAttribute("appinfo_port", appinfo_port);
        model.addAttribute("appinfo_agentId", appinfo_agentId);
        return page;
    }

}
