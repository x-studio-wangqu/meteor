package cn.com.ry.framework.application.meteor.meteor.meteorparam.web;

import cn.com.ry.framework.application.meteor.framework.Constants;
import cn.com.ry.framework.application.meteor.framework.exception.ValidationException;
import cn.com.ry.framework.application.meteor.framework.json.XjjJson;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecEdit;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecList;
import cn.com.ry.framework.application.meteor.framework.security.annotations.SecPrivilege;
import cn.com.ry.framework.application.meteor.framework.web.ManagerInfo;
import cn.com.ry.framework.application.meteor.framework.web.SpringControllerSupport;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.entity.MachineinfoEntity;
import cn.com.ry.framework.application.meteor.meteor.machineinfo.service.MachineinfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 参数设置
 */
@Controller
@RequestMapping("/meteor/param")
public class MeteorParamController extends SpringControllerSupport {
    @Autowired
    private MachineinfoService machineinfoService;


    @SecPrivilege(title = "服务器管理")
    @SecList
    @RequestMapping(value = "/index")
    public String index(Model model, HttpSession session) {
        String page = this.getViewPath("index");
        //获取参数
        //获取moduleNameList
        ManagerInfo loginInfo = (ManagerInfo) session.getAttribute(Constants.SESSION_MANAGER_INFO_KEY);
        Long userId = loginInfo.getUserId();
        List<String> moduleList = machineinfoService.findModuleList(userId);
        List<Map> moduleNameList = new ArrayList<>();
        if (null != moduleList && moduleList.size() > 0) {
            for (String moduleName : moduleList) {
                Map tempMap = new HashMap();
                tempMap.put("key", moduleName);
                tempMap.put("value", moduleName);
                moduleNameList.add(tempMap);
            }
        }
        session.setAttribute("moduleNameList", moduleNameList);
        session.setAttribute("hostList", new ArrayList());
        return page;
    }

    @SecEdit
    @RequestMapping(value = "/getMachineList/{moduleName}")
    @ResponseBody
    public List<MachineinfoEntity> getMachineList(@PathVariable("moduleName") String moduleName, HttpSession session) {
        ManagerInfo loginInfo = (ManagerInfo) session.getAttribute(Constants.SESSION_MANAGER_INFO_KEY);
        Long userId = loginInfo.getUserId();
        List<MachineinfoEntity> hostList = machineinfoService.findHostListByModule(userId, moduleName);
        return hostList;
    }


    @RequestMapping(value = "/saveMachineParam")
    @ResponseBody
    public XjjJson saveMachineParam(@RequestBody MachineinfoEntity machineinfoEntity, HttpSession session) {
        //保存 arthas 参数
        Long id = machineinfoEntity.getId();
        String moduleName = machineinfoEntity.getModuleName();
        if (null == id) {
            throw new ValidationException("该服务器ID不存在");
        }
        if (null != machineinfoEntity.getHostname()) {
            throw new ValidationException("该服务器名称为空");
        }
        ManagerInfo loginInfo = (ManagerInfo) session.getAttribute(Constants.SESSION_MANAGER_INFO_KEY);
        Long userId = loginInfo.getUserId();
        MachineinfoEntity machine = machineinfoService.findMachineByMachineId(userId, id, moduleName);
        this.getRequest().getSession().setAttribute("meteor_param", machine);
        return XjjJson.success("保存成功");
    }
}
